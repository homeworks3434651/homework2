package org.homework2;

import java.util.Random;

public class PlaceTargetOneCell {
    static int[] placeTarget() {
        Random random = new Random();
        int targetRow = random.nextInt(5) + 1;
        int targetCol = random.nextInt(5) + 1;
        return new int[]{targetRow, targetCol};
    }
}
