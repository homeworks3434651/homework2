package org.homework2;

import java.util.Scanner;

import static org.homework2.PlayHardGame.playHardGame;
import static org.homework2.PlaySimpleGame.playSimpleGame;

public class ShootingGame {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the game begin!");
        System.out.print("Enter your name: ");
        String name = scanner.nextLine();

        int userLevel;
        while (true) {
            System.out.print("Change the level (simple[1] or hard[2]): ");
            String userInput = scanner.nextLine().trim();
            if (userInput.matches("\\d+")) {
                userLevel = Integer.parseInt(userInput);
                if (userLevel == 1 || userLevel == 2) {
                    break;
                } else {
                    System.out.println("Invalid level selection. Please enter 1 or 2: ");
                }
            } else {
                System.out.println("Please enter a valid integer for the level.");
            }
        }

        if (userLevel == 1) {
            playSimpleGame(name);
        } else if (userLevel == 2) {
            playHardGame(name);
        }

        scanner.close();
    }
}


