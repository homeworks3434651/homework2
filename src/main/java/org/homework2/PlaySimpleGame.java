package org.homework2;

import static org.homework2.Board.*;
import static org.homework2.Guess.getGuess;
import static org.homework2.PlaceTargetOneCell.placeTarget;

public class PlaySimpleGame {

    static void playSimpleGame(String playerName){
        System.out.println(ConsoleColors.BLUE_BOLD_BRIGHT + "All Set. Get ready to rumble!" + ConsoleColors.RESET);
        char[][] board = initializeBoard();
        int[] targetPosition = placeTarget();

        while (true) {
            printBoard(board);
            int[] guess = getGuess(board);

            if (guess[0] == targetPosition[0] && guess[1] == targetPosition[1]) {
                board[guess[0]][guess[1]] = 'x';
                printBoard(board);
                System.out.println(
                        ConsoleColors.BLUE_BACKGROUND
                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                + "Congratulations, " + playerName + "! You have won!"
                                + ConsoleColors.RESET);
                break;
            } else {
                System.out.println(
                        ConsoleColors.RED_BACKGROUND_BRIGHT
                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                + "Missed!"
                                + ConsoleColors.RESET);
                board[guess[0]][guess[1]] = '*';
            }
        }
    }
}
