package org.homework2;

import java.util.Random;

public class PlaceTargetThreeCells {

    static int[][] placeTarget() {
        Random random = new Random();
        int orientation = random.nextInt(2); // 0 - horizontal, 1 - vertical

        int targetRow = random.nextInt(4) + 1;
        int targetCol = random.nextInt(4) + 1;

        int[][] targetPositions = new int[3][2];

        if (orientation == 0 && targetCol <= 3) {
            // Horizontal line
            for (int i = 0; i < 3; i++) {
                targetPositions[i][0] = targetRow;
                targetPositions[i][1] = targetCol + i;
            }
        } else if (orientation == 1 && targetRow <= 3) {
            // Vertical line
            for (int i = 0; i < 3; i++) {
                targetPositions[i][0] = targetRow + i;
                targetPositions[i][1] = targetCol;
            }
        } else {
            // Retry placement
            return placeTarget();
        }

        return targetPositions;
    }
}
