package org.homework2;

import static org.homework2.Board.*;
import static org.homework2.CheckWin.checkWin;
import static org.homework2.Guess.getGuess;
import static org.homework2.IsTargetHit.*;
import static org.homework2.PlaceTargetThreeCells.placeTarget;

public class PlayHardGame {

    static void playHardGame(String playerName) {
        System.out.println(ConsoleColors.BLUE_BOLD_BRIGHT + "All Set. Get ready to rumble!" + ConsoleColors.RESET);
        char[][] board = initializeBoard();
        int[][] targetPositions = placeTarget();
        int correctGuesses = 0;

        while (true) {
            printBoard(board);
            int[] guess = getGuess(board);

            if (isTargetHit(targetPositions, guess)) {
                markTargetOnBoard(board, targetPositions, guess);
                correctGuesses++;

                if (correctGuesses == 3 && checkWin(targetPositions, board)) {
                    printBoard(board);
                    System.out.println(
                            ConsoleColors.BLUE_BACKGROUND
                                    + ConsoleColors.BLACK_BOLD_BRIGHT
                                    + "Congratulations, " + playerName + "! You have won!"
                                    + ConsoleColors.RESET);

                    break;
                } else {
                    System.out.println(
                            ConsoleColors.GREEN_BACKGROUND
                                    + ConsoleColors.BLACK_BOLD_BRIGHT
                                    + "You have guessed the cell!"
                                    + ConsoleColors.RESET);
                }
            } else {
                System.out.println(
                        ConsoleColors.RED_BACKGROUND_BRIGHT
                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                + "Missed!"
                                + ConsoleColors.RESET);
                markMissOnBoard(board, guess);
            }
        }
    }
}
