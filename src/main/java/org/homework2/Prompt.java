package org.homework2;

import static org.homework2.Guess.rowAlreadyUsed;

public class Prompt {

    static String getRowPrompt(char[][] board) {
        StringBuilder prompt = new StringBuilder(ConsoleColors.YELLOW_BOLD_BRIGHT + "Enter row (");
        for (int i = 1; i < 6; i++) {
            if (!rowAlreadyUsed(board, i)) {
                prompt.append(i);
                prompt.append(", ");
            }
        }
        prompt.delete(prompt.length() - 2, prompt.length()); // Remove the last comma and space
        prompt.append("): " + ConsoleColors.RESET);
        return prompt.toString();
    }

    static String getColumnPrompt(char[][] board, int row) {
        StringBuilder prompt = new StringBuilder(ConsoleColors.PURPLE_BOLD_BRIGHT + "Enter column (");
        for (int i = 1; i < 6; i++) {
            if (board[row][i] != '*') {
                prompt.append(i);
                prompt.append(", ");
            }
        }
        prompt.delete(prompt.length() - 2, prompt.length()); // Remove the last comma and space
        prompt.append("): " + ConsoleColors.RESET);
        return prompt.toString();
    }
}
