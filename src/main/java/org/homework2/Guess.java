package org.homework2;

import java.util.Scanner;

import static org.homework2.Prompt.getColumnPrompt;
import static org.homework2.Prompt.getRowPrompt;

public class Guess {

    static int[] getGuess(char[][] board) {
        Scanner scanner = new Scanner(System.in);
        int[] guess = new int[2];

        while (true) {
            System.out.print(getRowPrompt(board));
            String inputRow = scanner.nextLine().trim();

            if (!inputRow.isEmpty() && inputRow.matches("\\d+")) {
                guess[0] = Integer.parseInt(inputRow);

                if (guess[0] >= 1 && guess[0] <= 5) {
                    if (rowAlreadyUsed(board, guess[0])) {
                        System.out.println("You have already used this row.");
                    } else {
                        break;
                    }
                }
            }
        }

        while (true) {
            System.out.print(getColumnPrompt(board, guess[0]));
            String inputCol = scanner.nextLine().trim();

            if (!inputCol.isEmpty() && inputCol.matches("\\d+")) {
                guess[1] = Integer.parseInt(inputCol);

                if (guess[1] >= 1 && guess[1] <= 5) {
                    if (cellAlreadyUsed(board, guess[0], guess[1])) {
                        System.out.println("You have already used this cell.");
                    } else {
                        break;
                    }
                }
            }
        }

        return guess;
    }

    static boolean rowAlreadyUsed(char[][] board, int row) {
        for (int col = 1; col < 6; col++) {
            if (board[row][col] == '-') {
                return false;  // Return false if at least one cell is available
            }
        }
        return true;  // Return true if all cells are used
    }

    private static boolean cellAlreadyUsed(char[][] board, int row, int col) {
        return board[row][col] != '-';
    }
}
