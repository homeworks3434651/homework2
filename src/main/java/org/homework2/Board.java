package org.homework2;

public class Board {

    static char[][] initializeBoard() {
        char[][] board = new char[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                board[i][j] = '-';
            }
        }
        return board;
    }

    static void printBoard(char[][] board) {
        for (int col = 0; col < 6; col++) {
            System.out.print(
                    ConsoleColors.CYAN_BACKGROUND
                            + ConsoleColors.BLACK_BOLD_BRIGHT + col + ConsoleColors.BLACK_BOLD_BRIGHT + " | "
                            + ConsoleColors.RESET);
        }
        System.out.println();
        System.out.println(
                ConsoleColors.CYAN_BACKGROUND
                        + ConsoleColors.WHITE_BOLD_BRIGHT + "-".repeat(24) + ConsoleColors.RESET);

        for (int row = 1; row < 6; row++) {
            System.out.print(
                    ConsoleColors.CYAN_BACKGROUND
                            + ConsoleColors.BLACK_BOLD_BRIGHT + row + " " + ConsoleColors.BLACK_BOLD_BRIGHT + ConsoleColors.RESET);

            for (int col = 1; col < 6; col++) {
                char cell = board[row][col];
                String cellColor = (cell == 'x') ? ConsoleColors.GREEN_BOLD :
                        (cell == '*') ? ConsoleColors.RED_BOLD : ConsoleColors.BLACK_BOLD_BRIGHT;
                String backgroundColor = (cell == 'x') ? ConsoleColors.BLUE_BACKGROUND_BRIGHT :
                        (cell == '*') ? ConsoleColors.YELLOW_BACKGROUND_BRIGHT : ConsoleColors.CYAN_BACKGROUND;
                System.out.printf("%s%s|%s", ConsoleColors.CYAN_BACKGROUND, ConsoleColors.BLACK_BOLD_BRIGHT, ConsoleColors.RESET);
                System.out.printf("%s%s %s %s", backgroundColor, cellColor, cell, ConsoleColors.RESET);
            }
            System.out.print(
                    ConsoleColors.CYAN_BACKGROUND
                            + ConsoleColors.BLACK_BOLD_BRIGHT + "| " + ConsoleColors.BLACK_BOLD_BRIGHT + ConsoleColors.RESET);
            System.out.println();
            System.out.println(
                    ConsoleColors.CYAN_BACKGROUND
                            + ConsoleColors.WHITE_BOLD_BRIGHT + "-".repeat(24) + ConsoleColors.RESET);
        }
    }
}
