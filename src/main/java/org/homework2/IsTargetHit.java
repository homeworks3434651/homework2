package org.homework2;

public class IsTargetHit {
    static boolean isTargetHit(int[][] targetPositions, int[] guess) {
        for (int i = 0; i < targetPositions.length; i++) {
            if (targetPositions[i][0] == guess[0] && targetPositions[i][1] == guess[1]) {
                return true;
            }
        }
        return false;
    }

    static void markTargetOnBoard(char[][] board, int[][] targetPositions, int[] guess) {
        for (int i = 0; i < targetPositions.length; i++) {
            int row = targetPositions[i][0];
            int col = targetPositions[i][1];
            if (row == guess[0] && col == guess[1]) {
                // If the guessed cell matches the target, mark it with 'x'
                board[row][col] = 'x';
            }
        }
    }

    static void markMissOnBoard(char[][] board, int[] guess) {
        board[guess[0]][guess[1]] = '*';
    }
}
