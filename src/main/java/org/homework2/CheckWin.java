package org.homework2;

public class CheckWin {
    static boolean checkWin(int[][] targetPositions, char[][] board) {
        int count = targetPositions.length;
        int hitsCount = 0;

        for (int i = 0; i < count; i++) {
            int targetRow = targetPositions[i][0];
            int targetCol = targetPositions[i][1];

            if (board[targetRow][targetCol] == 'x') {
                hitsCount++;

                if (hitsCount == count) {
                    return true;  // Player has won, all targets were hit
                }
            }
        }

        return false;  // Player has not won yet
    }
}
